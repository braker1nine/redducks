//
//  File.swift
//
//
//  Created by Chris Brakebill on 8/12/20.
//

import Foundation
import ReactiveSwift


@dynamicMemberLookup
public class Store<State: Equatable, Action> {

    /**
     * An array of functions to run before updating state. Middleware can run API methods and
     * dispatch new actions to update the state
     */
    private let middleware: [Middleware<State, Action>]

    /**
     * A mutable `ReactiveSwift`  representation of the app state
     *
     * This is kept private so external object can't edit the state. It should only
     * be editable through actions and `dispatch`
     */
    @MutableProperty private var internalState: State

    /**
     * The function to run actions against. It takes an `AppState` and updates it given an `Action`
     * and then returns a new `AppState`
     */
    private var reducer: Reducer<State, Action>

    /**
     * Exposed version of the state. This is where consumers can subscribe and receive updates to the app state
     */
    public lazy var state: Property<State> = .init(
        initial: self.internalState,
        then: self.$internalState.producer.observe(on: QueueScheduler.main)
    )

    /**
     * Exposes access to state values through key paths
     *
     * This allows consumers who simply need a value from a part of the state to pass in a keypath and return that value
     * Note this will not be reactive. So it's probably best used for passing as arguments to APIs or other methods that
     * aren't reactive.
     *
     * ```
     * store[keyPath:\.profile.driverType] // Returns the DriverType
     * ```
     * - parameter dynamicMember: A `KeyPath` object which pulls a value out of the state
     * - returns: The value of the state at that keypath
     */
    public subscript<T>(dynamicMember keyPath: KeyPath<State, T>) -> T {
        self.state.value[keyPath: keyPath]
    }

    /**
     * Dispatch method for handling events
     *
     * If middleware handles the action it can return true to indicate to the dispatch method that it will call
     * any supplied completion method when it decides it has completed
     */
    @MainActor
    public func dispatch(action: Action) async {

        self.internalState = self.reducer.send(state: self.internalState, action: action)
        
        /// Run each middleware function
        await self.middleware.asyncForEach({ middleware in
            await middleware.send(action: action, dispatch: self.dispatch(action:), getState: { self.internalState })
        })
    }
    
    public func dispatch(action: Action, completion: (() -> Void)? = nil) {
        Task(priority: .medium) {
            await self.dispatch(action: action)
            completion?()
        }
    }

    public init(state: State, middleware: [Middleware<State, Action>] = [], reducer: Reducer<State, Action>) {
        self.internalState = state
        self.middleware = middleware
        self.reducer = reducer
    }

    /// Extensions to allow for binding
    private lazy var lifetime = Lifetime(self.lifetimeToken)
    private lazy var lifetimeToken = Lifetime.Token()

    deinit {
        self.lifetimeToken.dispose()
    }
    
    public func scope<LocalState, LocalAction>(
        path: KeyPath<State, LocalState>,
        mapAction: @escaping (LocalAction) -> Action
    ) -> Store<LocalState, LocalAction> {
        let localStore = Store<LocalState, LocalAction>(
            state: self.state.value[keyPath: path],
            reducer: .init { _, action in
                Task {
                    await self.dispatch(action: mapAction(action))
                }
                return self.state.value[keyPath: path]
            }
        )
        
        return localStore
    }
}

///
/// Make the store conform to `BindingTargetProvider` so we can send actions directly into it
/// from a signal
///
/// ```
/// self.store <~ self.$query.map(AnActionType.query).toAnyAction()
/// ```
///
extension Store: BindingTargetProvider {
    public typealias Value = Action

    public var bindingTarget: BindingTarget<Action> {
        BindingTarget<Action>(
            lifetime: self.lifetime,
            action: { action in
                Task(priority: .high) {
                    await self.dispatch(action: action)
                }
            }
        )
    }
}
