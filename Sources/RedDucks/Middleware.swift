//
//  File.swift
//  
//
//  Created by Chris Brakebill on 8/12/20.
//

import Foundation
import ReactiveSwift

/**
 * Middleware is a protocol for running side effects from actions sent to the store
 * It is specifically focused on providing a hook for side effects like network requests or database calls
 *
 * It has a `send` method which receives  three parameters when called by the store
 *
 * action: `Action` The action that was dispatched to the store. Middleware uses this to decide whether it wants to respond
 *
 * dispatch: `Dispatch`
 * This is the `Store`'s dispatch method. This allows the responding middleware to easily send multiple events
 * to the store
 *
 * state: `AppState`
 * The current state of the store, in case the middle ware needs to look into state to run it's side effects
 */

open class Middleware<State: Equatable, Action> {
    open func send(action: Action, dispatch: @escaping Dispatch<Action>, getState: () -> State) async { }

    public init() {}
}

