//
//  File.swift
//
//
//  Created by Chris Brakebill on 8/12/20.
//

import Foundation

public enum ReducerError: Error {
    case dynamicCallKeyword

    var message: String {
        switch self {
        case .dynamicCallKeyword:
            return "You dynamically called with the wrong arguments. Must include state: State, and action: Action"
        }
    }
}

public struct Reducer<State: Equatable, Action> {

    /// I don't love these being asynchronous. Seems like a way to get unexpected side effects
    let block: (State, Action) -> State

    public static func combine(reducers: Reducer<State, Action>...) -> Reducer<State, Action> {

        return Reducer<State, Action> { (state, action) -> State in
            var state = state
            reducers.forEach { reducer in
                state = reducer.send(state: state, action: action)
            }
            return state
        }
    }

    public func pullback<GlobalState, GlobalAction>(
        keyPath: WritableKeyPath<GlobalState, State>,
        _ mapAction: @escaping (GlobalAction) -> Action?
    ) -> Reducer<GlobalState, GlobalAction> {
        return Reducer<GlobalState, GlobalAction> { (state, action) -> GlobalState in
            guard let localAction = mapAction(action) else { return state }
            var mutableGlobalState = state
            var localState = state[keyPath: keyPath]
            localState = self.block(localState, localAction)
            mutableGlobalState[keyPath: keyPath] = localState
            return mutableGlobalState
        }
    }
    
    public func send(state: State, action: Action) -> State {
        self.block(state, action)
    }

    public init(_ block: @escaping (State, Action) -> State) {
        self.block = block
    }
}
