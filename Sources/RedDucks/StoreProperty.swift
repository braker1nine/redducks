//
//  StoreProperty.swift
//  
//
//  Created by Chris Brakebill on 3/18/22.
//

import Foundation
import ReactiveSwift

/// Store Property is a Property wrapper that allows you to have a Redux backed property.
/// You can get values from it, observe it as a `Property` through `projectedValue`, and set it with as Action
@propertyWrapper
public struct StoreProperty<State: Equatable, Value, Action> {
    private let store: Store<State, Action>
    private let action: (Value) -> Action?
    private let path: KeyPath<State, Value>
    
    /// Intializes a `StoreProperty`
    /// - parameter store: The `Store` object to back the property with
    /// - parameter path: A `KeyPath` from the store's root state type to the type of the property you want to create
    /// - parameter action: A function which takes a `Value` type as a parameter and returns an optional `Action` for the store. This will be called when you set the value on this property
    public init(store: Store<State, Action>, path: KeyPath<State,Value>, action: @escaping (Value) -> Action?) {
        self.store = store
        self.action = action
        self.path = path
    }
    
    public var wrappedValue: Value {
        get {
            self.store.state.value[keyPath: self.path]
        }
        
        set {
            guard let action = self.action(newValue) else { return }
            let store = self.store
            Task {
                await store.dispatch(action: action)
            }
        }
    }
    
    public var projectedValue: Property<Value> {
        self.store.state.map(self.path)
    }
    
    public func set(value: Value) async {
        guard let action = self.action(value) else { return }
        await store.dispatch(action: action)
    }
}
