//
//  WithStore.swift
//  
//
//  Created by Chris Brakebill on 3/18/22.
//

#if os(iOS)

import Foundation
import UIKit
import ReactiveSwift

/// **Experimental**
/// In scenarios where you're using DSL it allows you to wrap UI building code and supply it with state + Dispatch target
/// This might be best added to another target that's something like `RedDucks+Diesel`
///
func WithStore<State, Action>(
    _ store: Store<State, Action>,
    block: (Property<State>, @escaping Dispatch<Action>) -> UIView
) -> UIView {
    return block(store.state, store.dispatch(action:))
}

#endif
