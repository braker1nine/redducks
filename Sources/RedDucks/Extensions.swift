//
//  File.swift
//  
//
//  Created by Chris Brakebill on 3/14/22.
//

import Foundation

extension Sequence {
    func asyncForEach(
        _ operation: (Element) async throws -> Void
    ) async rethrows {
        for element in self {
            try await operation(element)
        }
    }
}
