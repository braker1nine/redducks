//
//  File.swift
//  
//
//  Created by Chris Brakebill on 8/12/20.
//

import Foundation

/**
 * `Action` is an empty protocol. It is simply used to declare values that can be dispatched to the store
 */

/// Dispatch is a method that takes an action and returns void
public typealias Dispatch<Action> = (Action) async -> Void
