//
//  ReducerTests.swift
//  
//
//  Created by Chris Brakebill on 3/14/22.
//

import XCTest
@testable import RedDucks

struct State: Equatable {
    var text: String
    var number: Int
    var ownedValue: Int
}

enum StateAction {
    case setText(String)
    case setNumber(Int)
    case setOwnedValue(Int)
}



class ReducerTests: XCTestCase {
    
    var globalReducer: Reducer<State, StateAction> = .combine(
        reducers:
            Reducer<Int, Int> { state, action in action }.pullback(
                keyPath: \.number, {
                    switch $0 {
                    case .setNumber(let number):
                        return number
                    default:
                        return nil
                    }
                }
            ),
        Reducer<String, String> { state, action in action }.pullback(keyPath: \.text, { action in
            switch action  {
            case .setText(let text):
                return text
            default:
                return nil
            }
        }),
        Reducer<State, StateAction> { state, action in
            
            switch action {
            case .setOwnedValue(let value):
                var state = state
                state.ownedValue = value
                return state
            default:
                return state
            }
        }
    )

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFirstAction() throws  {
        let state = globalReducer.send(
            state: State(text: "Test", number: 1, ownedValue: 2),
            action: StateAction.setText("New Text")
        )
        
        XCTAssertEqual(state.text, "New Text")
        XCTAssertEqual(state.number, 1)
        XCTAssertEqual(state.ownedValue, 2)
    }

    
    func testSecondAction() throws {
        let state = globalReducer.send(
            state: State(text: "Test", number: 1, ownedValue: 2),
            action: .setNumber(2345)
        )
        
        XCTAssertEqual(state.text, "Test")
        XCTAssertEqual(state.number, 2345)
        XCTAssertEqual(state.ownedValue, 2)
    }
    
    func testOwnedAction() throws {
        let state = globalReducer.send(
            state: State(text: "Test", number:1, ownedValue: 2),
            action: .setOwnedValue(42)
        )
        
        XCTAssertEqual(state.text, "Test")
        XCTAssertEqual(state.number, 1)
        XCTAssertEqual(state.ownedValue, 42)
    }

}
