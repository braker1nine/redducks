//
//  StoreTests.swift
//  
//
//  Created by Chris Brakebill on 3/14/22.
//

import XCTest
@testable import RedDucks

enum StoreAction {
    case increment(Int)
    case decrement(Int)
}

class StoreTests: XCTestCase {
    
    var store: Store<Int, StoreAction>!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        store = Store<Int, StoreAction>.init(
           state: 0, reducer: .init({ value, action in
               switch action {
               case .increment(let change):
                   return value + change
               case .decrement(let change):
                   return value - change
               }
           }))
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIncrement() async throws {
        await store.dispatch(action: .increment(1))
        
        XCTAssertEqual(store.state.value, 1)
    }

    func testDecrement() async {
        await store.dispatch(action: .decrement(10))
        XCTAssertEqual(store.state.value, -10)
    }

}
